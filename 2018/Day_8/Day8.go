package main

import (
	"fmt"
	"strings"
	"strconv"
	"io/ioutil"
)

type Node struct {
	NumChildren int
	NumMetadata int
	Children []Node
	Metadata []int
}

func main(){
	input, err := ioutil.ReadFile("Day8.txt")
	if err != nil {
		panic(err)
	}
	nums := strings.Split(strings.TrimSpace(string(input)), " ")
	Tree, _ := ParseChild(nums, 0)

	sum := 0
	for _, m := range GetChildMetadata(Tree){
		sum += m
	}

	fmt.Printf("P1Answer: %d\n", sum)
	fmt.Printf("P2Answer: %d\n", GetChildValue(Tree))

}

func ParseChild(nums []string, pos int) (Node, int) {
	n := Node{}
	n.NumChildren, _ = strconv.Atoi(nums[pos+0])
	n.NumMetadata, _ = strconv.Atoi(nums[pos+1])
	pos += 2
	ch := []Node{}

	for j := 0; j < n.NumChildren; j++ {
		newCh, k := ParseChild(nums, pos)
		pos = k
		ch = append(ch, newCh)
	}
	n.Children = ch

	for _, m := range nums[pos: pos + n.NumMetadata] {
		meta, _ := strconv.Atoi(m)
		n.Metadata = append(n.Metadata, meta)
	}

	pos += n.NumMetadata
	return n, pos
}

func GetChildMetadata(N Node) (meta []int) {
	for _, c := range N.Children {
		for _, m := range GetChildMetadata(c){
			meta = append(meta, m)
		}
	}
	for _, m := range N.Metadata {
		meta = append(meta, m)
	}
	return
}

func GetChildValue(N Node) (v int){
	if len(N.Children)  == 0 {
		for _, c := range N.Metadata {
			v += c
		}
	} else {
		for _, m := range N.Metadata {
			if len(N.Children) > m - 1 {
				v += GetChildValue(N.Children[m - 1])
			}
		}
	}
	return
}
