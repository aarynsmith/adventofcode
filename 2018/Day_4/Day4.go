package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

type logType int

const (
	_ logType = iota
	startShift
	fallAsleep
	wakeUp
)

type logEntry struct {
	Time  time.Time
	Entry string
	Type  logType
	ID    int
	State bool
}

type guardLog struct {
	minuteTally map[int]int
	sleepTally  int
}

func main() {
	dat, err := ioutil.ReadFile("Day4.txt")
	if err != nil {
		panic(err)
	}
	log := strings.Split(string(dat), "\n")

	Log := []logEntry{}
	for _, entry := range log {
		if len(entry) < 17 {
			continue
		}
		t, err := time.Parse("2006-01-02 15:04", entry[1:17])
		if err != nil {
			panic(err)
		}
		Log = append(Log, logEntry{
			Time:  t,
			Entry: entry,
		})
	}
	sort.Slice(Log, func(i, j int) bool { return Log[i].Time.Before(Log[j].Time) })

	CurrentGuard := 0
	SleepState := false
	LogType := logType(0)
	GuardRegexp := regexp.MustCompile(`Guard #(\d+) begins shift`)
	for i, entry := range Log {
		switch t := entry.Entry[19:24]; t {
		case "Guard":
			m := GuardRegexp.FindStringSubmatch(entry.Entry)
			if m == nil {
				panic("Couldn't find Guard number")
			}
			CurrentGuard, _ = strconv.Atoi(m[1])
			LogType = startShift
		case "falls":
			SleepState = true
			LogType = fallAsleep
		case "wakes":
			SleepState = false
			LogType = wakeUp
		default:
			panic("Unknown Status")
		}
		Log[i].ID = CurrentGuard
		Log[i].State = SleepState
		Log[i].Type = LogType
	}

	Guards := map[int]guardLog{}
	sleepTimeStart := 0
	for _, entry := range Log {
		if entry.Type == fallAsleep {
			sleepTimeStart = entry.Time.Minute()
		}
		if entry.Type == wakeUp {
			Gl := guardLog{minuteTally: map[int]int{}}
			if _, o := Guards[entry.ID]; o {
				Gl = Guards[entry.ID]
			}
			for i := sleepTimeStart; i < entry.Time.Minute(); i++ {
				Gl.minuteTally[i]++
			}
			Gl.sleepTally += entry.Time.Minute() - sleepTimeStart
			Guards[entry.ID] = Gl
		}
	}

	Part1Count, P1Answer := 0, 0
	Part2Count, P2Answer := 0, 0
	for k, v := range Guards {
		maxMin, maxMinCount := 0, 0
		for i := 0; i < 60; i++ {
			if v.minuteTally[i] > maxMinCount {
				maxMin, maxMinCount = i, v.minuteTally[i]
			}
			if v.minuteTally[i] > Part2Count {
				Part2Count, P2Answer = v.minuteTally[i], k*i
			}
		}
		if v.sleepTally > Part1Count {
			Part1Count, P1Answer = v.sleepTally, k*maxMin
		}
	}
	fmt.Printf("Part 1 Answer: %d\nPart 2 Answer: %d", P1Answer, P2Answer)
}
