package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

//Cell is the representation of each acre of the Lumber Collection Area
type Cell int8

//Theses are the three states of Cells
const (
	Open Cell = iota
	Tree
	Lumber
)

//ParseCell coverts a rune representation of a cell to it's Cell counterpart
func ParseCell(b rune) Cell {
	switch b {
	case '.':
		return Cell(0)
	case '|':
		return Cell(1)
	case '#':
		return Cell(2)
	}
	return -1
}

func (c Cell) String() string {
	return map[Cell]string{
		Open:   ".",
		Tree:   "|",
		Lumber: "#",
	}[c]
}

//LCA represents a grid of cells represented by our scan of the Lumber Collection Area
type LCA [][]Cell

func (l *LCA) String() (s string) {
	for i := 0; i < len(*l); i++ {
		for j := 0; j < len((*l)[i]); j++ {
			s += (*l)[i][j].String()
		}
		s += "\n"
	}
	return
}

//CountAdjacent will count how many of the specified type of cell are adjacent to the x, y coordinates given.
func (l *LCA) CountAdjacent(q Cell, x, y int) (s int) {
	for tX := x - 1; tX <= x+1; tX++ {
		for tY := y - 1; tY <= y+1; tY++ {
			if tX == x && tY == y {
				continue
			}
			if tX < 0 || tY < 0 || tX >= len(*l) || tY >= len((*l)[tX]) {
				continue
			}
			if (*l)[tX][tY] == q {
				s++
			}
		}
	}
	return
}

//Tick will process one minute of growth for a lumber collection area
func (l *LCA) Tick() {
	PreTick := l.Copy()
	for x := 0; x < len(*l); x++ {
		for y := 0; y < len((*l)[x]); y++ {
			c := (*l)[x][y]
			switch c {
			case Open:
				if PreTick.CountAdjacent(Tree, x, y) >= 3 {
					(*l)[x][y] = Tree
				}
			case Tree:
				if PreTick.CountAdjacent(Lumber, x, y) >= 3 {
					(*l)[x][y] = Lumber
				}
			case Lumber:
				if PreTick.CountAdjacent(Lumber, x, y) >= 1 &&
					PreTick.CountAdjacent(Tree, x, y) >= 1 {
					(*l)[x][y] = Lumber
				} else {
					(*l)[x][y] = Open
				}
			}
		}
	}
}

//TickN Grows the lumberyard N number of times.
func (l *LCA) TickN(n int) {
	for i := 0; i < n; i++ {
		l.Tick()
	}
}

//Copy creates a copy of the LCA.
func (l *LCA) Copy() *LCA {
	s := LCAFromString(l.String())
	return &s
}

//LCAFromString creates a Lumber Collection Area from a string
func LCAFromString(s string) (l LCA) {
	lines := strings.Split(strings.TrimSpace(s), "\n")
	l = make(LCA, len(lines))
	for i := 0; i < len(lines); i++ {
		// fmt.Printf("Parsing Line: %s", string(lines[i]))
		l[i] = make([]Cell, len(strings.TrimRight(lines[i], "\r\n")))
		for j, c := range strings.TrimRight(lines[i], "\r\n") {
			l[i][j] = ParseCell(c)
		}
	}
	return
}

//FromString updates a Lumber Collection Area from a string
func (l *LCA) FromString(s string) {
	lines := strings.Split(strings.TrimSpace(s), "\n")
	//l = make(LCA, len(lines))
	for i := 0; i < len(lines); i++ {
		// fmt.Printf("Parsing Line: %s", string(lines[i]))
		//l[i] = make([]Cell, len(strings.TrimRight(lines[i], "\r\n")))
		for j, c := range strings.TrimRight(lines[i], "\r\n") {
			(*l)[i][j] = ParseCell(c)
		}
	}
	return
}

//Value returns the value of a lumber collection area as defined by the problem
//Number of treed acres multiplied by Number of Lumberyards
func (l *LCA) Value() int {
	Trees, Lumberyards := 0, 0
	for x := 0; x < len(*l); x++ {
		for y := 0; y < len((*l)[x]); y++ {
			switch (*l)[x][y] {
			case Tree:
				Trees++
			case Lumber:
				Lumberyards++
			}
		}
	}
	return Trees * Lumberyards
}

//TickNLoop Ticks an LCA for N minutes, memoizing results in the event of a loop
func (l *LCA) TickNLoop(n int) {
	memo := map[string][]int{}
	loop := map[int]string{}
	loopStart, loopEnd := -1, -1
	for i := 0; i < n; i++ {
		s := l.String()
		if _, ok := memo[s]; ok {
			if len(memo[s]) >= 2 {
				loopStart, loopEnd = memo[s][0], memo[s][1]
				// fmt.Printf("Seen At %v\n", memo[s])
				break
			}
		}
		memo[s] = append(memo[s], i)
		l.Tick()
	}
	if loopStart == -1 {
		return
	}
	for i := loopStart; i < loopEnd; i++ {
		l.Tick()
		loop[i-loopStart] = l.String()

	}
	r := (n - loopStart - 1) % len(loop)
	l.FromString(loop[r])

}

func main() {
	CollectionArea := ReadInput("Day18.txt")
	P1 := CollectionArea.Copy()
	P1.TickN(10)
	fmt.Printf("P1Answer: %d\n", P1.Value())
	P2 := CollectionArea.Copy()
	P2.TickNLoop(1000000000)
	fmt.Printf("P2Answer: %d", P2.Value())
}

//ReadInput takes a file name and returns two slices of strings containing the first part and second part of the puzzle input.
func ReadInput(fn string) *LCA {
	dat, err := ioutil.ReadFile(fn)
	if err != nil {
		panic(err)
	}
	l := LCAFromString(string(dat))
	return &l
}
