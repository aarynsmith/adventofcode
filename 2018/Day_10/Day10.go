package main

import (
	"io/ioutil"
	"strings"
	"strconv"
	"fmt"
	"regexp"
)

type Point struct {
	x,y int
	vx, vy int
}

func main() {
	dat, err := ioutil.ReadFile("Day10.txt")
	if err != nil { panic(err) }
	lines := strings.Split(string(dat), "\n")

	Points := []Point{}

	PointRegexp := regexp.MustCompile(
		`position=<\s?([-0-9]+),\s\s?([-0-9]+)>\s` +
		`velocity=<\s?([-0-9]+),\s\s?([-0-9]+)>`,
	)
	for i, l := range lines {
		if len(l) == 0 { continue }
		m := PointRegexp.FindStringSubmatch(l)
		if m == nil { panic(fmt.Errorf("Line %d: No match found",i)) }
		x, err := strconv.Atoi(m[1])
		if err != nil { panic(err) }
		y, err := strconv.Atoi(m[2])
		if err != nil { panic(err) }
		vx, err := strconv.Atoi(m[3])
		if err != nil { panic(err) }
		vy, err := strconv.Atoi(m[4])
		if err != nil { panic(err) }
		Points = append(Points, Point{
			x: x,
			y: y,
			vx: vx,
			vy: vy,
		})
	}

	x,y := CalcSize(Points)
	s := x * y
	prevS, prevPoints := s-1, Points
	i := 0
	for {
		prevS, prevPoints = s, Points
		MovePoints(Points)
		i += 1
		x, y = CalcSize(Points)
		s = x * y
		if s > prevS {
			break
		}
	}

	Msg := RenderPoints(prevPoints)
	fmt.Printf("P1Answer:\n%s\n",Msg)
	fmt.Printf("P2Answer: %d\n", i)
}

func RenderPoints(Points []Point) (msg string) {
	minx, maxx, miny, maxy := 1<<63 - 1, -1 << 63, 1<<63 -1, -1 << 63
	for _, p := range Points {
		if p.x < minx { minx = p.x }
		if p.y < miny { miny = p.y }
		if p.x > maxx { maxx = p.x }
		if p.y > maxy { maxy = p.y }
	}
	gMaxX := abs(maxx) + abs(minx)
	gMaxY := abs(maxy) + abs(miny)

	Grid := make([][]string, gMaxX+1)
	for i := range Grid {
		Grid[i] = make([]string, gMaxY+1)
	}

	for _, p := range Points{
		Grid[p.x - abs(minx)][p.y - abs(miny)] = "*"
	}

	for y := 0; y <= gMaxY; y++{
		for x := 0; x <= gMaxX; x++{
			msg += fmt.Sprintf("%1s", Grid[x][y])
		}
		msg += fmt.Sprintf("\n")
	}
	msg, trim := "", msg
	for _, l := range strings.Split(trim, "\n") {
		if strings.Contains(l, "*") {
			msg += strings.TrimSpace(l)+"\n"
		}
	}
	return
}

func CalcSize(Points []Point) (gMaxX, gMaxY int) {
	minx, maxx, miny, maxy := 1<<63 - 1, -1 << 63, 1<<63 -1, -1 << 63
	for _, p := range Points {
		if p.x < minx { minx = p.x }
		if p.y < miny { miny = p.y }
		if p.x > maxx { maxx = p.x }
		if p.y > maxy { maxy = p.y }
	}
	gMaxX = abs(maxx) + abs(minx)
	gMaxY = abs(maxy) + abs(miny)
	return
}

func MovePoints(Points []Point){
	for i := 0; i < len(Points); i++{
		Points[i].x += Points[i].vx
		Points[i].y += Points[i].vy
	}
}

func abs(i int) int {
	if i < 0 {
		return i * -1
	}
	return i
}
