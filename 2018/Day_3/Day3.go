package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type claim struct {
	ClaimNo int
	x, y    int
	dx, dy  int
}

func main() {
	dat, err := ioutil.ReadFile("Day3.txt")
	if err != nil {
		panic(err)
	}
	claims := strings.Split(string(dat), "\n")

	Cloth := make([][]int, 1000)
	for i := range Cloth {
		Cloth[i] = make([]int, len(Cloth))
	}

	for _, claim := range claims {
		c := parseClaim(claim)
		if c == nil {
			continue
		}
		for x := c.x; x < c.x+c.dx; x++ {
			for y := c.y; y < c.y+c.dy; y++ {
				if Cloth[x][y] == 0 {
					Cloth[x][y] = c.ClaimNo
				} else {
					Cloth[x][y] = -1
				}
			}
		}
	}

	c := 0
	for i := range Cloth {
		for j := range Cloth[i] {
			if Cloth[j][i] == -1 {
				c++
			}
		}
	}
	fmt.Printf("%d overlapping inches\n", c)

	for _, claim := range claims {
		c := parseClaim(claim)
		if c == nil {
			continue
		}
		Clean := true
	Loop:
		for x := c.x; x < c.x+c.dx; x++ {
			for y := c.y; y < c.y+c.dy; y++ {
				if Cloth[x][y] != c.ClaimNo {
					Clean = false
					break Loop
				}
			}
		}
		if Clean {
			fmt.Printf("Claim %d is clean.\n", c.ClaimNo)
		}
	}
}

func parseClaim(c string) *claim {
	claimRegexp := regexp.MustCompile(`#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)`)
	m := claimRegexp.FindStringSubmatch(c)
	if m == nil {
		return nil
	}

	cn, _ := strconv.Atoi(m[1])
	x, _ := strconv.Atoi(m[2])
	y, _ := strconv.Atoi(m[3])
	dx, _ := strconv.Atoi(m[4])
	dy, _ := strconv.Atoi(m[5])

	claim := claim{
		ClaimNo: cn,
		x:       x,
		y:       y,
		dx:      dx,
		dy:      dy,
	}

	return &claim
}
