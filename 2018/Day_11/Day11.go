package main

import (
	"fmt"
)

func main() {
	SerialNumber := 7857

	Grid := make([][]int, 300)
	for i := range Grid {
		Grid[i] = make([]int, 300)
	}

	for y := range Grid {
		for x := range Grid[y] {
			Grid[x][y] = calcPowerLevel(x+1, y+1, SerialNumber)
		}
	}

	fmt.Printf("P1Answer: %s\n", findMax(Grid, 3, SerialNumber))
	fmt.Printf("P2Answer: %s\n", findMax(Grid, 300, SerialNumber))

}

func calcPowerLevel(x, y, s int) int {
	rID := x + 10
	return ((((rID*y + s) * rID) / 100) % 10) - 5
}

func calcSquare(x, y, s, q int, Grid [][]int) (sum int) {
	if x+q > 300 {
		return
	}
	if y+q > 300 {
		return
	}
	for xT := x; xT < x+q; xT++ {
		if xT > 300 {
			break
		}
		for yT := y; yT < y+q; yT++ {
			if yT > 300 {
				break
			}
			sum += Grid[xT-1][yT-1]
		}
	}
	return sum
}

func findMax(Grid [][]int, MaxSq int, SerialNumber int) string {
	Loc, Size, MaxPL := "", 0, -30
	for y := 1; y <= 300; y++ {
		for x := 1; x <= 300; x++ {
			for q := 1; q <= MaxSq; q++ {
				if x+q >= 300 || y+q >= 300 {
					continue
				}
				var p int
				p = calcSquare(x, y, SerialNumber, q, Grid)
				if p > MaxPL {
					MaxPL, Size, Loc = p, q, fmt.Sprintf("%d,%d", x, y)
				}
			}
		}
	}
	return fmt.Sprintf("%s,%d", Loc, Size)
}
