package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	dat, err := ioutil.ReadFile("Day5.txt")
	if err != nil {
		panic(err)
	}
	Seq := strings.TrimSpace(string(dat))

	OrigSeq := Seq

	Seq = Reduce(Seq, Components(OrigSeq))

	fmt.Printf("P1Answer: %d\n", len(Seq))

	Comp := Components(OrigSeq)
	P2Count := len(OrigSeq)
	for _, c := range Comp {
		newSeq := strings.Replace(OrigSeq, c, "", -1)
		newSeq = strings.Replace(newSeq, strings.ToLower(c), "", -1)
		newSeq = Reduce(newSeq, Comp)

		if P2Count > len(newSeq) {
			P2Count = len(newSeq)
		}
	}
	fmt.Printf("P2Answer: %d", P2Count)

}

func Components(s string) []string {
	r := map[string]bool{}
	for _, c := range s {
		r[strings.ToUpper(string(c))] = true
	}
	keys := make([]string, 0, len(r))
	for k, _ := range r {
		keys = append(keys, k)
	}
	return keys
}

func Reduce(s string, comp []string) string {
	var origLen = len(s)
	for _, c := range comp {
		s = strings.Replace(s, c + strings.ToLower(c), "", -1)
		s = strings.Replace(s, strings.ToLower(c) + c, "", -1)
	}
	if origLen > len(s) {
		s = Reduce(s, comp)
	}
	return s
}
