package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	dat, err := ioutil.ReadFile("Day2.txt")
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(dat), "\n")

	twoCount, threeCount := 0, 0
	for _, i := range lines {
		letterMap := map[rune]int{}
		for _, l := range i {
			letterMap[l]++
		}
		twoWord, threeWord := 0, 0
		for _, v := range letterMap {
			if v == 2 {
				twoWord = 1
			}
			if v == 3 {
				threeWord = 1
			}
		}
		twoCount += twoWord
		threeCount += threeWord
	}
	fmt.Printf("Checksum: %d * %d = %d\n", twoCount, threeCount, twoCount*threeCount)

	for _, wordOne := range lines {
		for _, wordTwo := range lines {
			if wordOne == wordTwo || len(wordOne) != len(wordTwo) {
				continue
			}
			diff, comb := compareWords(wordOne, wordTwo)
			if diff < 2 {
				fmt.Printf("Found Lockers: %s\n", comb)
				return
			}
		}
	}

}

func compareWords(a, b string) (diff int, comb string) {
	for i := 0; i < len(a); i++ {
		if b[i] != a[i] {
			diff++
		} else {
			comb = comb + string(a[i])
		}
	}
	return diff, comb
}
