package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"strconv"
)

type Point struct {
	x, y int
}

type Zone struct {
	area int
	isInf bool
}

func main() {
	dat, err := ioutil.ReadFile("Day6.txt")
	if err != nil {
		panic(err)
	}
	pts := strings.Split(string(dat), "\n")

	Points := []Point{}
	Zones := map[Point]Zone{}
	maxXY := 0
	for _, p := range pts{
		if len(p) == 0 {
			continue
		}
		pts := strings.Split(strings.TrimSpace(p), ", ")
		x, _ := strconv.Atoi(pts[0])
		y, _ := strconv.Atoi(pts[1])
		point := Point{ x: x, y: y }
		Zones[point] = Zone{ }
		Points = append(Points, point)
		if maxXY < x { maxXY = x }
		if maxXY < y { maxXY = y }
	}

	for y := 0; y < maxXY; y++ {
		for x := 0; x < maxXY; x++ {
			pts := ClosestPoint(x, y, Points)
			if len(pts) == 1 {
				z := Zones[pts[0]]
				z.area += 1
				if y == 0 || y == maxXY || x == 0 || x == maxXY {
					z.isInf = true
				}
				Zones[pts[0]] = z
			}
		}
	}

	LargestIntZone := 0
	for _,v := range Zones {
		if !v.isInf && LargestIntZone < v.area {
			LargestIntZone = v.area
		}
	}
	fmt.Printf("P1Answer: %d\n", LargestIntZone)

	P2Area := 0
	for y := 0; y < maxXY; y++ {
		for x := 0; x < maxXY; x++ {
			d := DistToAll(x, y, Points)
			if d < 10000 {
				P2Area += 1
			}
		}
	}
	fmt.Printf("P2Answer: %d\n", P2Area)
}

func DistToAll(x,y int, p []Point) (int) {
	sum := 0
	for _, q := range p {
		d := abs(x - q.x) + abs(y - q.y)
		sum += d
	}
	return sum
}

func ClosestPoint(x,y int, p []Point) (Points []Point) {
	Dists := map[Point]int{}
	ShortestDist := -1
	for _, q := range p {
		d := abs(x - q.x) + abs(y - q.y)
		Dists[q] = d
		if ShortestDist == -1 || d < ShortestDist {
			ShortestDist = d
		}
	}
	for k,v := range Dists{
		if v == ShortestDist{
			Points = append(Points, k)
		}
	}
	return Points
}

func abs(i int) int {
	if i < 0 {
		return i * -1
	}
	return i
}
