package main

import (
	"io/ioutil"
	"fmt"
	"container/list"
	"regexp"
	"strconv"
)

func main(){
	dat, err := ioutil.ReadFile("Day9.txt")
	if err != nil { panic(err) }

	inpRegexp := regexp.MustCompile(`(\d+) players; last marble is worth (\d+) points`)
	m := inpRegexp.FindStringSubmatch(string(dat))
	if m == nil { panic("Couldn't Parse Input") }

	players, err := strconv.Atoi(m[1])
	if err != nil { panic(err) }
	marbles, err := strconv.Atoi(m[2])
	if err != nil { panic(err) }

	PlayerScores := PlayGame(players, marbles)
	P1Answer := 0
	for _, n := range PlayerScores {
		if n > P1Answer { P1Answer = n }
	}
	fmt.Printf("P1Answer: %d\n", P1Answer)

	PlayerScores = PlayGame(players, marbles * 100)
	P2Answer := 0
	for _, n := range PlayerScores {
		if n > P2Answer { P2Answer = n }
	}
	fmt.Printf("P2Answer: %d\n", P2Answer)
}

func CircPrev(l *list.List, p *list.Element, i int) *list.Element {
	for j := 0; j < i; j++ {
		p = p.Prev()
		if p == nil { p = l.Back() }
	}
	return p
}

func CircNext(l *list.List, p *list.Element, i int) *list.Element {
	for j := 0; j < i; j++ {
		p = p.Next()
		if p == nil { p = l.Front() }
	}
	return p
}

func PlayGame(Players, Marbles int) (PlayerScores map[int]int) {
	player := 0
	PlayerScores = map[int]int{}
	l := list.New()
	l.PushFront(0)
	p := l.Front()
	for i := 1; i <= Marbles; i++ {
		player += 1
		if player > Players { player = 1}

		if i % 23 == 0 {
			p = CircPrev(l, p, 8)
			r := p
			p = CircNext(l, p, 2)
			PlayerScores[player] += r.Value.(int) + i
			l.Remove(r)
			continue
		}
		l.InsertAfter(i,p)
		p = CircNext(l,p,2)
	}
	return
}
