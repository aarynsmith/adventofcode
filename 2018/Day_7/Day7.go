package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sort"
)

type Task struct {
	Name string
	Prereq []string
}

type Worker struct {
	CurrentOp Task
	TimeRemaining int
}

func main() {
	dat, err := ioutil.ReadFile("Day7.txt")
	if err != nil {
		panic(err)
	}
	input := strings.Split(string(dat), "\n")

	Tasks := map[string]Task{}
	for _, s := range input {
		if len(s) < 36 {
			continue
		}
		pre, step := string(s[5]), string(s[36])

		st := Task{ Name: step,  }
		if _, ok := Tasks[step]; ok {
			st = Tasks[step]
		}
		if _, ok := Tasks[pre]; !ok {
			Tasks[pre] = Task{ Name: pre }
		}

		st.Prereq = append(st.Prereq, pre)

		Tasks[step] = st
	}

	Complete := []string{}
	Available := AvailableTasks(Tasks, Complete)
	for len(Available) > 0 {
		Complete = append(Complete, Available[0])
		Available = AvailableTasks(Tasks, Complete)
	}
	fmt.Printf("P1Answer: %s\n", strings.Join(Complete, ""))

	Complete = []string{}
	Available =  []string{}
	S := 0
	Worker := []Worker{
		Worker{ CurrentOp: Task{}},
		Worker{ CurrentOp: Task{}},
		Worker{ CurrentOp: Task{}},
		Worker{ CurrentOp: Task{}},
		Worker{ CurrentOp: Task{}},
	}

	for len(Complete) < len(Tasks){
		for i := range Worker {
			if Worker[i].TimeRemaining == 0 && Worker[i].CurrentOp.Name != "" {
				Complete = append(Complete, Worker[i].CurrentOp.Name)
				Worker[i].CurrentOp = Task{}
			}
		}
		for i := range Worker {
			if Worker[i].CurrentOp.Name == "" {
				Available = NotInProgress(AvailableTasks(Tasks, Complete), Worker)
				if len(Available) > 0 {
					Worker[i].CurrentOp, Available = Tasks[Available[0]], Available[1:]
					Worker[i].TimeRemaining = int(Worker[i].CurrentOp.Name[0])-64 + 60
				}
			}
		}
		S += 1
		for i := range Worker {
			Worker[i].TimeRemaining -= 1
		}
	}
	fmt.Printf("P2Answer: %d\n", S-1)
}

func NotInProgress(Availible []string, Workers []Worker) []string {
	nip := []string{}
	for _, s := range Availible {
		InProgress := false
		for _, w := range Workers {
			if s == w.CurrentOp.Name { InProgress = true }
		}
		if !InProgress { nip = append(nip, s) }
	}
	return nip
}

func AvailableTasks(Tasks map[string]Task, Complete []string) (Available []string) {
	for k := range Tasks {
		if contains(Complete, k) { continue }
		av := true
		for _, pre := range Tasks[k].Prereq {
			if !contains(Complete, pre){ av = false }
		}
		if av { Available = append(Available, k) }
	}
	sort.Strings(Available)
	return
}

func contains(s []string, v string) bool {
	for _, k := range s {
		if k == v {
			return true
		}
	}
	return false
}
