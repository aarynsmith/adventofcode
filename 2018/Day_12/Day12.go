package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main(){
	dat, err := ioutil.ReadFile("Day12.txt")
	if err != nil { panic(err) }
	lines := strings.Split(string(dat), "\n")

	initState := lines[0]
	lines = lines[2:]

	rules := map[string]string{}
	for _, r := range lines {
		if r == "" {
			continue
		}
		m := strings.Split(strings.TrimSpace(r), " => ")
		if len(m) < 2 {
			continue
		}
		rules[m[0]] = m[1]
	}

	Pots := []string{}
	for _, i := range strings.TrimSpace(initState[15:]) {
		Pots = append(Pots, string(i))
	}

	Generation := 0
	PotZero := 0
	for Generation < 20 {
		Pots, PotZero = Grow(Pots, rules, PotZero)
		Generation++
	}

	P1Answer := 0
	for i := range Pots {
		if Pots[i] == "#" {
			P1Answer += i - PotZero
		}
	}
	fmt.Printf("P1Answer: %d\n", P1Answer)


	P1x, P1y := Generation, P1Answer - 87
	for Generation < 1000 { 
		Pots, PotZero = Grow(Pots, rules, PotZero)
		Generation++
	}
	P2Answer := 0
	for i := range Pots {
		if Pots[i] == "#" {
			P2Answer += i - PotZero
		}
	}
	P2x, P2y := Generation, P2Answer - 87

	M := ((P2y-P1y) / (P2x-P1x))+1
	B := P2y - (M * P2x) + 87

	// fmt.Printf(" Y = %d * X + %d\n", M, B)

	fmt.Printf("P2Answer: %d\n", M * 50000000000 + B)

}

func CheckAdjacent(p []string, pos int) (s string) {
	for i := pos - 2; i <= pos + 2; i++ {
		s += GetValue(p, i)
	}
	return s
}

func GetValue(p []string, pos int) string {
	if pos < 0 { return "." }
	if pos >= len(p) { return "." }
	return p[pos]
}

func Grow(p []string, rules map[string]string, Zero int) ([]string, int) {
	np := []string{}
	for i := -4; i < len(p) + 4; i++ {
		Adj := CheckAdjacent(p, i)
		Rule := rules[Adj]
		if Rule == "" { Rule = "." }
		if i < 0 && Rule != "." {
			Zero += 1
			np = append(np, Rule)
		}
		if i >= 0 {
			np = append(np, Rule)
		}
	}
	return np, Zero
}
